#ifndef SUBJECT_H
#define SUBJECT_H

#include "Observer.h"

template <class EVENT>
class Subject {
public:
	virtual ~Subject() {}
	virtual void attach(Observer<EVENT>& observer) = 0;
	virtual void notify(const EVENT& event) = 0;
};

#endif
